#!/bin/bash

for file in [1-9]*
do
	nawk 'BEGIN {increment=1}{printf "%d \n", $1+1}END{}' $file >> corr-$file
done

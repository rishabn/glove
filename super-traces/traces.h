#ifndef TRACES_H
#define TRACES_H

#include <iostream>
#include <vector>
#include "packets.h"

struct trace
{
	int type;
	long long int length, up_bytes, down_bytes, total_bytes, play_time;
	std::vector<packet> packets;
};

void getTrace(trace tr)
{
	for(int i = 0 ; i < tr.packets.size() ; i ++)
		std::cout<<"Packet: "<<i+1<<", Direction: "<<tr.packets[i].direction<<", Size: "<<tr.packets[i].size<<", Time: "<<tr.packets[i].time<<std::endl;	
}

trace setTrace(std::vector<packet> packets, int type)
{
	trace tr;
	tr.type = type,	tr.length = packets.size(), tr.up_bytes = 0, tr.down_bytes = 0;	
	for(int i = 0 ; i < packets.size() ; i ++)
	{
		tr.packets.push_back(packets[i]);
		if(packets[i].direction == 1)
			tr.down_bytes += abs(packets[i].size);
		else
			tr.up_bytes += abs(packets[i].size);
	}
	tr.play_time = packets[packets.size() - 1].time;
	tr.total_bytes = abs(tr.up_bytes + tr.down_bytes);
	return tr;
}

void printTraceChars(trace t)
{
	std::cout<<"Trace Length: "<<t.length<<", Bytes: "<<t.total_bytes<<", TTC: "<<t.play_time<<"\n";
}

#endif

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "fileio.h"

using namespace std;

int main()
{

	/*
		Create cluster.txt
		#websites, #clusters	eg., 500, 54
		site#, cluster#		eg., 1 25
	*/
	ofstream cluster, st;
	ifstream input_cluster, input_st;
	
	stringstream filename;
	vector<int> cluster_map(500);
	for(int i = 1 ; i < 54 ; i ++)
	{
		filename.str("");
		filename<<"../clusters/54_clusters/"<<i<<".54";
		input_cluster.open(filename.str());
		while(!input_cluster.eof())
		{
			string temp;
			getline(input_cluster, temp);
			int arg = atoi(temp.c_str());
			cluster_map[arg] = i;
		}
		input_cluster.close();
	}
	cluster.open("cluster.txt", ios::out);
	cluster<<"500, 53"<<endl;
	for(int i = 0 ; i < cluster_map.size() ; i ++)
	{
		cluster<<i<<", "<<cluster_map[i]<<endl;
	}
	cluster.close();

	/*
		Create st.txt
		cluster#, #packets
		packet sizes
		packet times
	*/

	stringstream bytes, times;
	st.open("st.txt", ios::out);
	for(int i = 1 ; i < 54 ; i ++)
	{
		bytes.str("");
		times.str("");
		bytes<<"./st_output/54/"<<i<<".bytes";
		times<<"./st_output/54/"<<i<<".times";
		trace t = read_from_disk(bytes.str(), times.str());
		st<<i<<", "<<t.length<<endl;
		for(int i = 0 ; i < t.length ; i ++)
			st<<t.packets[i].direction*t.packets[i].size<<", ";
		st<<endl;
		st<<fixed;
		for(int i = 0 ; i < t.length ; i ++)
			st<<t.packets[i].time<<", ";
		st<<endl;
	}
	st.close();
	return 0;
}



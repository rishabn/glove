#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <iterator>

using namespace std;

int main(int argc, char **argv)
{
	int start_row = stoi(argv[1]), end_row = stoi(argv[2]);
	vector<long long int> row(10000);
	vector<vector<long long int>> matrix;

	for(int i = 0 ; i < row.size() ; i ++)
		row[i] = -1;
	for(int i = 0 ; i < row.size() ; i ++)
		matrix.push_back(row);
	for(int i = start_row ; i < end_row ; i ++)
		matrix[i][i] = 0;

	ifstream in;
	ofstream out;
	in.open("./FastDTW-logs/distances");
	stringstream filename;
	filename<<"matrix-"<<start_row<<"-"<<end_row;
	out.open(filename.str(), ios::trunc);
	long long int read_i, read_j, read_distance, counter = 0;
	string temp;
	while(!in.eof())
	{
		getline(in, temp);
		vector<long long int>read(3);
		istringstream iss(temp);
		copy(istream_iterator<long long int>(iss),istream_iterator<long long int>(),read.begin());
		matrix[read[0]][read[1]] = read[2];
		matrix[read[1]][read[0]] = read[2];
	}
	in.close();
	stringstream command_stream;
	string inter;
	for(int i = start_row ; i < end_row ; i ++)
	{
		for(int j = 0 ; j < row.size() ; j ++)
		{
			if(matrix[i][j] == -1)
			{
				int status = chdir("./FastDTW-1.1.0/src");
				command_stream.str("");
				command_stream<<"java com.FastDtwTest ../../gen_ts/2401_"<<i<<"_down.ts ../../gen_ts/2401_"<<j<<"_down.ts 100 "<<start_row;
				string command = command_stream.str();
				status = system(command.c_str());
				command_stream.str("");
				command_stream<<start_row;
				ifstream tempfile(command_stream.str());
				command_stream.str("");
				getline(tempfile, inter);
				matrix[i][j] = atoll(inter.c_str());
				matrix[j][i] = atoll(inter.c_str());
				tempfile.close();
				command_stream<<"rm "<<start_row;
				command = command_stream.str();
				status = system(command.c_str());
				status = chdir("../../");	
				counter++;
			}
			out<<matrix[i][j]<<" ";
		}
		out<<endl;
	}
	out.close();
	return 0;
}


# In[50]:

import sklearn
from sklearn import *
from numpy import *
import sys
from scipy.cluster.vq import *

get_ipython().magic(u'pylab inline')

dir_ = './de_tailed_rep_traces/'
from os import path

size_array = np.array([], dtype = float_)
time_array = np.array([], dtype = float_)
len_array = np.zeros([501], dtype = float_)
up_array = np.zeros([501], dtype = float_)
down_array = np.zeros([501], dtype = float_)
ttc_array = np.zeros([501], dtype = float_)

trace_list_size = list()
trace_list_time = list()

for i in range(1, 501): 
    size_p =  str(i) + '_1.size'
    time_p =  str(i) + '_1.time'
    size_path = path.join(dir_, size_p)
    time_path = path.join(dir_, time_p)
    temp_size = [float(l.strip()) for l in open(size_path).read().splitlines()]
    temp_time = [float(l.strip()) for l in open(time_path).read().splitlines()]
    if len(temp_size) != len(temp_time):
        print 'ERROR: UNEQUAL TRACES' + str(i)
    start_time = temp_time[0];
    for j in range(0, len(temp_time)):
        temp_time[j] = (temp_time[j] - start_time)
        ttc_array[i] = temp_time[j]
        if(temp_size[j] < 0):
            up_array[i] = up_array[i] + abs(temp_size[j])
        else:
            down_array[i] = down_array[i] + temp_size[j]
    trace_list_size.append(temp_size)
    trace_list_time.append(temp_time)
    len_array[i] = len(temp_size)


# Out[50]:

#     Populating the interactive namespace from numpy and matplotlib
# 

# In[51]:

#Time step of 5ms for the timeseries
time_series_upstream = list()
time_series_downstream = list()
intervals = np.zeros([501], dtype=int)
cut_off = 5000

for i in range(1, 501):
    temp = int(trace_list_time[i-1][int(len_array[i])-1]/cut_off)+2
    intervals[i] = temp
    temp_up = np.zeros([intervals[i]])
    temp_down = np.zeros([intervals[i]])
    count = 0
    for j in range(1, intervals[i]):
        for k in range(0, len(trace_list_time[i-1])):
            if trace_list_time[i-1][k] <= j*cut_off:
                if trace_list_size[i-1][k] < 0:
                    temp_up[j-1] = temp_up[j-1] + abs(trace_list_size[i-1][k])
                else:
                    temp_down[j-1] = temp_down[j-1] + abs(trace_list_size[i-1][k])
    time_series_downstream.append(temp_down)
    time_series_upstream.append(temp_up)


# In[52]:

for i in range(1, 501):
    print 'At site: '+str(i)
    filename_up = 'site_'+str(i)+'.upseries'
    filename_down = 'site_'+str(i)+'.downseries'
    temp_up = open(filename_up, 'w')
    temp_down = open(filename_down, 'w')
    for j in range(0, len(time_series_upstream[i-1])):
        temp_up.write(str(time_series_upstream[i-1][j])+'\n') 
        temp_down.write(str(time_series_downstream[i-1][j])+'\n') 
    temp_up.close()
    temp_down.close()


# Out[52]:

#     At site: 1
#     At site: 2
#     At site: 3
#     At site: 4
#     At site: 5
#     At site: 6
#     At site: 7
#     At site: 8
#     At site: 9
#     At site: 10
#     At site: 11
#     At site: 12
#     At site: 13
#     At site: 14
#     At site: 15
#     At site: 16
#     At site: 17
#     At site: 18
#     At site: 19
#     At site: 20
#     At site: 21
#     At site: 22
#     At site: 23
#     At site: 24
#     At site: 25
#     At site: 26
#     At site: 27
#     At site: 28
#     At site: 29
#     At site: 30
#     At site: 31
#     At site: 32
#     At site: 33
#     At site: 34
#     At site: 35
#     At site: 36
#     At site: 37
#     At site: 38
#     At site: 39
#     At site: 40
#     At site: 41
#     At site: 42
#     At site: 43
#     At site: 44
#     At site: 45
#     At site: 46
#     At site: 47
#     At site: 48
#     At site: 49
#     At site: 50
#     At site: 51
#     At site: 52
#     At site: 53
#     At site: 54
#     At site: 55
#     At site: 56
#     At site: 57
#     At site: 58
#     At site: 59
#     At site: 60
#     At site: 61
#     At site: 62
#     At site: 63
#     At site: 64
#     At site: 65
#     At site: 66
#     At site: 67
#     At site: 68
#     At site: 69
#     At site: 70
#     At site: 71
#     At site: 72
#     At site: 73
#     At site: 74
#     At site: 75
#     At site: 76
#     At site: 77
#     At site: 78
#     At site: 79
#     At site: 80
#     At site: 81
#     At site: 82
#     At site: 83
#     At site: 84
#     At site: 85
#     At site: 86
#     At site: 87
#     At site: 88
#     At site: 89
#     At site: 90
#     At site: 91
#     At site: 92
#     At site: 93
#     At site: 94
#     At site: 95
#     At site: 96
#     At site: 97
#     At site: 98
#     At site: 99
#     At site: 100
#     At site: 101
#     At site: 102
#     At site: 103
#     At site: 104
#     At site: 105
#     At site: 106
#     At site: 107
#     At site: 108
#     At site: 109
#     At site: 110
#     At site: 111
#     At site: 112
#     At site: 113
#     At site: 114
#     At site: 115
#     At site: 116
#     At site: 117
#     At site: 118
#     At site: 119
#     At site: 120
#     At site: 121
#     At site: 122
#     At site: 123
#     At site: 124
#     At site: 125
#     At site: 126
#     At site: 127
#     At site: 128
#     At site: 129
#     At site: 130
#     At site: 131
#     At site: 132
#     At site: 133
#     At site: 134
#     At site: 135
#     At site: 136
#     At site: 137
#     At site: 138
#     At site: 139
#     At site: 140
#     At site: 141
#     At site: 142
#     At site: 143
#     At site: 144
#     At site: 145
#     At site: 146
#     At site: 147
#     At site: 148
#     At site: 149
#     At site: 150
#     At site: 151
#     At site: 152
#     At site: 153
#     At site: 154
#     At site: 155
#     At site: 156
#     At site: 157
#     At site: 158
#     At site: 159
#     At site: 160
#     At site: 161
#     At site: 162
#     At site: 163
#     At site: 164
#     At site: 165
#     At site: 166
#     At site: 167
#     At site: 168
#     At site: 169
#     At site: 170
#     At site: 171
#     At site: 172
#     At site: 173
#     At site: 174
#     At site: 175
#     At site: 176
#     At site: 177
#     At site: 178
#     At site: 179
#     At site: 180
#     At site: 181
#     At site: 182
#     At site: 183
#     At site: 184
#     At site: 185
#     At site: 186
#     At site: 187
#     At site: 188
#     At site: 189
#     At site: 190
#     At site: 191
#     At site: 192
#     At site: 193
#     At site: 194
#     At site: 195
#     At site: 196
#     At site: 197
#     At site: 198
#     At site: 199
#     At site: 200
#     At site: 201
#     At site: 202
#     At site: 203
#     At site: 204
#     At site: 205
#     At site: 206
#     At site: 207
#     At site: 208
#     At site: 209
#     At site: 210
#     At site: 211
#     At site: 212
#     At site: 213
#     At site: 214
#     At site: 215
#     At site: 216
#     At site: 217
#     At site: 218
#     At site: 219
#     At site: 220
#     At site: 221
#     At site: 222
#     At site: 223
#     At site: 224
#     At site: 225
#     At site: 226
#     At site: 227
#     At site: 228
#     At site: 229
#     At site: 230
#     At site: 231
#     At site: 232
#     At site: 233
#     At site: 234
#     At site: 235
#     At site: 236
#     At site: 237
#     At site: 238
#     At site: 239
#     At site: 240
#     At site: 241
#     At site: 242
#     At site: 243
#     At site: 244
#     At site: 245
#     At site: 246
#     At site: 247
#     At site: 248
#     At site: 249
#     At site: 250
#     At site: 251
#     At site: 252
#     At site: 253
#     At site: 254
#     At site: 255
#     At site: 256
#     At site: 257
#     At site: 258
#     At site: 259
#     At site: 260
#     At site: 261
#     At site: 262
#     At site: 263
#     At site: 264
#     At site: 265
#     At site: 266
#     At site: 267
#     At site: 268
#     At site: 269
#     At site: 270
#     At site: 271
#     At site: 272
#     At site: 273
#     At site: 274
#     At site: 275
#     At site: 276
#     At site: 277
#     At site: 278
#     At site: 279
#     At site: 280
#     At site: 281
#     At site: 282
#     At site: 283
#     At site: 284
#     At site: 285
#     At site: 286
#     At site: 287
#     At site: 288
#     At site: 289
#     At site: 290
#     At site: 291
#     At site: 292
#     At site: 293
#     At site: 294
#     At site: 295
#     At site: 296
#     At site: 297
#     At site: 298
#     At site: 299
#     At site: 300
#     At site: 301
#     At site: 302
#     At site: 303
#     At site: 304
#     At site: 305
#     At site: 306
#     At site: 307
#     At site: 308
#     At site: 309
#     At site: 310
#     At site: 311
#     At site: 312
#     At site: 313
#     At site: 314
#     At site: 315
#     At site: 316
#     At site: 317
#     At site: 318
#     At site: 319
#     At site: 320
#     At site: 321
#     At site: 322
#     At site: 323
#     At site: 324
#     At site: 325
#     At site: 326
#     At site: 327
#     At site: 328
#     At site: 329
#     At site: 330
#     At site: 331
#     At site: 332
#     At site: 333
#     At site: 334
#     At site: 335
#     At site: 336
#     At site: 337
#     At site: 338
#     At site: 339
#     At site: 340
#     At site: 341
#     At site: 342
#     At site: 343
#     At site: 344
#     At site: 345
#     At site: 346
#     At site: 347
#     At site: 348
#     At site: 349
#     At site: 350
#     At site: 351
#     At site: 352
#     At site: 353
#     At site: 354
#     At site: 355
#     At site: 356
#     At site: 357
#     At site: 358
#     At site: 359
#     At site: 360
#     At site: 361
#     At site: 362
#     At site: 363
#     At site: 364
#     At site: 365
#     At site: 366
#     At site: 367
#     At site: 368
#     At site: 369
#     At site: 370
#     At site: 371
#     At site: 372
#     At site: 373
#     At site: 374
#     At site: 375
#     At site: 376
#     At site: 377
#     At site: 378
#     At site: 379
#     At site: 380
#     At site: 381
#     At site: 382
#     At site: 383
#     At site: 384
#     At site: 385
#     At site: 386
#     At site: 387
#     At site: 388
#     At site: 389
#     At site: 390
#     At site: 391
#     At site: 392
#     At site: 393
#     At site: 394
#     At site: 395
#     At site: 396
#     At site: 397
#     At site: 398
#     At site: 399
#     At site: 400
#     At site: 401
#     At site: 402
#     At site: 403
#     At site: 404
#     At site: 405
#     At site: 406
#     At site: 407
#     At site: 408
#     At site: 409
#     At site: 410
#     At site: 411
#     At site: 412
#     At site: 413
#     At site: 414
#     At site: 415
#     At site: 416
#     At site: 417
#     At site: 418
#     At site: 419
#     At site: 420
#     At site: 421
#     At site: 422
#     At site: 423
#     At site: 424
#     At site: 425
#     At site: 426
#     At site: 427
#     At site: 428
#     At site: 429
#     At site: 430
#     At site: 431
#     At site: 432
#     At site: 433
#     At site: 434
#     At site: 435
#     At site: 436
#     At site: 437
#     At site: 438
#     At site: 439
#     At site: 440
#     At site: 441
#     At site: 442
#     At site: 443
#     At site: 444
#     At site: 445
#     At site: 446
#     At site: 447
#     At site: 448
#     At site: 449
#     At site: 450
#     At site: 451
#     At site: 452
#     At site: 453
#     At site: 454
#     At site: 455
#     At site: 456
#     At site: 457
#     At site: 458
#     At site: 459
#     At site: 460
#     At site: 461
#     At site: 462
#     At site: 463
#     At site: 464
#     At site: 465
#     At site: 466
#     At site: 467
#     At site: 468
#     At site: 469
#     At site: 470
#     At site: 471
#     At site: 472
#     At site: 473
#     At site: 474
#     At site: 475
#     At site: 476
#     At site: 477
#     At site: 478
#     At site: 479
#     At site: 480
#     At site: 481
#     At site: 482
#     At site: 483
#     At site: 484
#     At site: 485
#     At site: 486
#     At site: 487
#     At site: 488
#     At site: 489
#     At site: 490
#     At site: 491
#     At site: 492
#     At site: 493
#     At site: 494
#     At site: 495
#     At site: 496
#     At site: 497
#     At site: 498
#     At site: 499
#     At site: 500
# 

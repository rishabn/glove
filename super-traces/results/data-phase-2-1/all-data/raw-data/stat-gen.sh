#!/bin/bash 

rm cumulative-*

for min in 10 15 20 25 30 35 40 
do
	for tau in 33 50 66 99
	do
		for k in 16 29 54 81 104 131 153 172 200 225 250
		do
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-clusters-$k-min-$min
			echo $min $tau $k >> cumulative-clusters-$k-min-$min
#			echo '\n' >> cumulative-clusters-$k-min-$min
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-clusters-$k-tau-$tau
			echo $min $tau $k >> cumulative-clusters-$k-tau-$tau
#			echo '\n' >> cumulative-clusters-$k-tau-$tau
		
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-min-$min-clusters-$k
			echo $min $tau $k >> cumulative-min-$min-clusters-$k
#			echo '\n' >> cumulative-min-$min-clusters-$k
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-min-$min-tau-$tau
			echo $min $tau $k >> cumulative-min-$min-tau-$tau
#			echo '\n' >> cumulative-min-$min-tau-$tau

			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-tau-$tau-clusters-$k
			echo $min $tau $k >> cumulative-tau-$tau-clusters-$k
#			echo '\n' >> cumulative-tau-$tau-clusters-$k
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-tau-$tau-min-$min
			echo $min $tau $k >> cumulative-tau-$tau-min-$min
#			echo '\n' >> cumulative-tau-$tau-min-$min

			#printf "\n" >> cumulative
		done
	done
done


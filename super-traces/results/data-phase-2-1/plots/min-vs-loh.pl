# Gnuplot script file for plotting results in file "force.dat"
set terminal postscript color enhanced eps 
set output 'plot-min-vs-loh.ps'
#set size 1,0.5;
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label   
set grid 
set xrange [20:80]
set yrange [0:10]
set y2range [00:100]
set xtic 10                          # set xtics automatically
set ytic 1                          # set ytics automatically
set y2tic 10
#set multiplot layout 1, 2 ;

#set title "Effect of {/Symbol m} _{min} on Bandwidth Ratio and {/Symbol m} _{avg} [ k=54, n=500 ]" font ",10"
#set size 0.5,0.5;
set key default
set key inside samplen 3 spacing 2 font ",18"
#set key box
set key top left #box
#set key out vert top right
set xlabel "{/Symbol m} _{min} (%)" font ",20"
set ylabel "Latency Ratio (x)" font ",20"
set y2label "{/Symbol m} _{avg} (%)" font ",20"
set xtics font "Times-Roman, 18" 
set ytics font "Times-Roman, 18" 
set y2tics font "Times-Roman, 18" 

set style line 1 lt 1 lw 6 pt 6 lc rgb "red"
set style line 2 lt 1 lw 6 pt 6 lc rgb "blue"
set style line 3 lt 1 lw 6 pt 6 lc rgb "black"
set style line 4 lt 1 lw 6 pt 6 lc rgb "green"

plot    "../aggregated-results/cumulative-tau-99-clusters-54" using (2* $13):(100* $12) w lines ls 1 title "Average Coverage ({/Symbol m} _{avg})" axes x1y2, "../aggregated-results/cumulative-tau-99-clusters-54" using (2* $13):($9) w lines ls 2 title "Glove ({/Symbol e} = .108, {/Symbol t} = 99)" axes x1y1, "../aggregated-results/cumulative-tau-33-clusters-54" using (2*$13):($9) w lines ls 4 title "Glove ({/Symbol e} = .108, {/Symbol t} = 33)" axes x1y1


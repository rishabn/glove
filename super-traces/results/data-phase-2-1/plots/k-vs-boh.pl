# Gnuplot script file for plotting data in file "force.dat"
set terminal postscript color enhanced eps 
#set output 'plot-1-a.ps'
#set size 1,0.5;
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label   
set grid 
set xrange [0:50]
set yrange [0:6]
set xtic 10                          # set xtics automatically
set ytic .5                          # set ytics automatically
#set multiplot layout 1, 2 ;

#set title "Effect of {/Symbol m} _{min} on Bandwidth Ratio and {/Symbol m} _{avg} [ k=54, n=500 ]" font ",10"
#set size 0.5,0.5;
set key default
set key inside samplen 3 spacing 2 font ",18"
#set key box
set key top left #box
#set key out vert top right
set xlabel "Best Attack Accuracy (%)" font ",20"
set ylabel "Bandwidth Ratio (x)" font ",20"
set xtics font "Times-Roman, 18" 
set ytics font "Times-Roman, 18" 

set style line 1 lt 1 lw 6 pt 6 lc rgb "red"
set style line 2 lt 1 lw 6 pt 6 lc rgb "blue"
set style line 3 lt 1 lw 6 pt 6 lc rgb "black"
set style line 4 lt 1 lw 6 pt 6 lc rgb "green"

set output 'plot-k-vs-boh.ps'

#plot "../aggregated-results/cumulative-tau-99-min-25" using ($15*.002):($8) w lines ls 1 title "Security vs Bandwidth Ratio [{/Symbol t} = 99, {/Symbol m}_{min}= 25]" axes x1y1,  "../aggregated-results/cumulative-tau-99-min-30" using ($15*.002):($8) w lines ls 2 title "Security vs Bandwidth Ratio [{/Symbol t} = 99, {/Symbol m}_{min}= 30]" axes x1y1,  "../aggregated-results/cumulative-tau-99-min-35" using ($15*.002):($8) w lines ls 3 title "Security vs Bandwidth Ratio [{/Symbol t} = 99, {/Symbol m}_{min}= 35]" axes x1y1, "../aggregated-results/cumulative-tau-99-min-40" using ($15*.002):($8) w lines ls 4 title "Security vs Bandwidth Ratio [{/Symbol t} = 99, {/Symbol m}_{min}= 40]" axes x1y1,   

plot "../aggregated-results/cumulative-tau-99-min-40" using ($15*.2):($8) w lines ls 1 title "Glove [{/Symbol t} = 99, {/Symbol m}_{min}= 40] vs. any attack" axes x1y1, "../aggregated-results/buflo" using ($2*100):($1) w lines ls 2 title "BuFLO vs. Panchenko", "../aggregated-results/csbuflo" using ($2*100):($1) title "CS-BuFLO vs. DL-SVM"

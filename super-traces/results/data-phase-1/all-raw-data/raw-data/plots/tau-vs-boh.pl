# Gnuplot script file for plotting data in file "force.dat"
set terminal postscript color enhanced eps 
set output 'plot-2.ps'
#set size 1,0.5;
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label   
set grid 
set xrange [10:100]
set yrange [0:10]
set xtic 10                          # set xtics automatically
set ytic 1                          # set ytics automatically
#set multiplot layout 1, 2 ;

set title "Effect of {/Symbol t} on Bandwidth and Latency Ratios [ k=54, n=500, {/Symbol m} _{min} = 30 ]" font ",10"
#set size 0.5,0.5;
set key default
set key inside samplen 2 spacing 1 font ",8"
#set key box
set key bot right #box
#set key out vert top right
set xlabel "{/Symbol t}" font ",8"
set ylabel "Bandwidth/Latency Ratio (x)" font ",8"

set xtics font "Times-Roman, 8" 
set ytics font "Times-Roman, 8" 
 

set style line 1 lt 1 lw 3 pt 3 lc rgb "red"
set style line 2 lt 1 lw 3 pt 3 lc rgb "blue"
set style line 3 lt 1 lw 3 pt 3 lc rgb "black"
set style line 4 lt 3 lw 3 pt 3 lc rgb "blue"

plot    "cumulative-tau-vs-oh" using ($14):($8) w lines ls 1 title "Bandwidth Ratio" axes x1y1, "cumulative-tau-vs-oh" using ($14):($9) w lines ls 2 title "Latency Ratio" axes x1y1

#set title "Effect of Minimum Coverage on Latency Overhead and Average Coverage" font ",8"
#set size 0.5,0.5;
#set key default
#set key inside samplen 2 spacing 1 font ",5"
#set key box
#set key bot right #box
#set key out vert top right
#set xlabel "Minimum Coverage (%)" font ",8"
#set ylabel "Latency Ratio (x)" font ",8"
#set y2label "Average Coverage (%)" font ",8"
#set xtics font "Times-Roman, 8" 
#set ytics font "Times-Roman, 8" 
#set y2tics font "Times-Roman, 8" 
#plot    "cumulative-33" using (2* $13):(100* $12) w lines ls 1 title "Average Site Coverage" axes x1y2, "cumulative-33" using (2* $13):($9) w lines ls 3 title "Latency Ratio" axes x1y1#, "cumulative-99" using (2*$13):($9) w lines ls 3 title "Latency Ratio" axes x1y1



# Gnuplot script file for plotting data in file "force.dat"
set terminal postscript color enhanced eps 
set output 'plot-1-a.ps'
#set size 1,0.5;
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label   
set grid 
set xrange [0:100]
set yrange [0:10]
set y2range [00:100]
set xtic 10                          # set xtics automatically
set ytic 1                          # set ytics automatically
set y2tic 10
#set multiplot layout 1, 2 ;

set title "Effect of {/Symbol m} _{min} on Bandwidth Ratio and {/Symbol m} _{avg} [ k=54, n=500 ]" font ",10"
#set size 0.5,0.5;
set key default
set key inside samplen 2 spacing 1 font ",8"
#set key box
set key bot right #box
#set key out vert top right
set xlabel "{/Symbol m} _{min} (%)" font ",8"
set ylabel "Bandwidth Ratio (x)" font ",8"
set y2label "{/Symbol m} _{avg} (%)" font ",8"
set xtics font "Times-Roman, 8" 
set ytics font "Times-Roman, 8" 
set y2tics font "Times-Roman, 8" 

set style line 1 lt 1 lw 3 pt 3 lc rgb "red"
set style line 2 lt 1 lw 3 pt 3 lc rgb "blue"
set style line 3 lt 1 lw 3 pt 3 lc rgb "black"
set style line 4 lt 3 lw 3 pt 3 lc rgb "blue"

plot    "cumulative-Tau-99" using (2* $13):(100* $12) w lines ls 1 title "{/Symbol m} _{avg}" axes x1y2, "cumulative-Tau-99" using (2* $13):($8) w lines ls 2 title "Bandwidth Ratio ({/Symbol t} = 99)" axes x1y1, "cumulative-Tau-66" using (2*$13):($8) w lines ls 3 title "Bandwidth Ratio ({/Symbol t} = 66)" axes x1y1, "cumulative-Tau-33" using (2*$13):($8) w lines ls 4 title "Bandwidth Ratio ({/Symbol t} = 33)" axes x1y1

#set title "Effect of Minimum Coverage on Latency Overhead and Average Coverage" font ",8"
#set size 0.5,0.5;
#set key default
#set key inside samplen 2 spacing 1 font ",5"
#set key box
#set key bot right #box
#set key out vert top right
#set xlabel "Minimum Coverage (%)" font ",8"
#set ylabel "Latency Ratio (x)" font ",8"
#set y2label "Average Coverage (%)" font ",8"
#set xtics font "Times-Roman, 8" 
#set ytics font "Times-Roman, 8" 
#set y2tics font "Times-Roman, 8" 
#plot    "cumulative-33" using (2* $13):(100* $12) w lines ls 1 title "Average Site Coverage" axes x1y2, "cumulative-33" using (2* $13):($9) w lines ls 3 title "Latency Ratio" axes x1y1#, "cumulative-99" using (2*$13):($9) w lines ls 3 title "Latency Ratio" axes x1y1



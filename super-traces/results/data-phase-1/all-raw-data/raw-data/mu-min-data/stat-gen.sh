#!/bin/bash 

rm cumulative*
for min in 2 5 10 20 30 40 42 45 48 
do
	for tau in 33 66 99
	do
		for k in 54 #100 149
		do
			nawk 'BEGIN { spbnorm=0; ipbnorm=0; sum1=0; sum2=0; sum3=0; sum4=0; sum5=0; sum6=0; sum7=0; boh=0; loh=0; cov=0} {sum1 += $1;sum2 += $2;sum3 += $3;sum4 += $4;sum5 += $5;sum6 += $6;sum7 += $7; } END { cov=sum2/25000; stbnorm=sum4/sum2; ipbnorm=sum5/sum2; boh=sum4/sum5; loh=sum6/sum7; printf "%d %d %d %d %d %d %d %f %f %f %f %f ", sum1, sum2, sum3, sum4, sum5, sum6, sum7, boh, loh, stbnorm, ipbnorm, cov } ' raw-results-$min-$tau-$k-1.cf >> cumulative-$tau
			echo $min $tau $k >> cumulative-$tau
			#printf "\n" >> cumulative
		done
	done
done


#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "traces.h"
#include "packets.h"
#include "fileio.h"
#include "supertrace_logic.h"

using namespace std;

int main(int argc, char **argv)
{
        if(argc < 7)
        {
		cout<<"USAGE: "<<endl<<"ARG 1: <site-coverage>, ARG 2: <cluster-no>, ARG 3: <no-clusters>, ARG 4: <cluster-path>, ARG 5: <rep-path>, ARG 6: <time-percentile>, ARG 7: <is-max>"<<endl;
                return 1;
        }
	//cout<<"Parameters: "<<endl;
        int coverage = atoi(argv[1]), cluster_no = atoi(argv[2]), no_clusters = atoi(argv[3]), percentile = atoi(argv[6]), is_max = atoi(argv[7]);
	string cluster_path = argv[4], rep_path = argv[5];
	trace ismax_trace, notismax_trace;
	//cout<<"Coverage: "<<coverage<<", ClusterID: "<<cluster_no<<", #Clusters: "<<no_clusters<<", Cluster Path: "<<cluster_path<<", Rep Path: "<<rep_path<<", Tau: "<<percentile<<", ISMax: "<<is_max<<endl;
	//cout<<"[LOG] Using IS_MAX: "<<is_max<<endl;
	ismax_trace = FrontierST(coverage, cluster_no, no_clusters, cluster_path, rep_path, percentile, is_max);
	//cout<<"\n\n[LOG] Using IS_MAX: "<<1-is_max<<endl;
	//notismax_trace = FrontierST(coverage, cluster_no, no_clusters, cluster_path, rep_path, percentile, 1-is_max);
	stringstream output_bytes, output_times;
	output_bytes.str("");
	output_times.str("");
	output_bytes<<"./st_output/"<<no_clusters<<"/"<<cluster_no<<".bytes";
	output_times<<"./st_output/"<<no_clusters<<"/"<<cluster_no<<".times";
	//if(notismax_trace.total_bytes < ismax_trace.total_bytes)
	//	write_to_disk(notismax_trace, output_bytes.str(), output_times.str());
	//else
		write_to_disk(ismax_trace, output_bytes.str(), output_times.str());
        return 0;
}
                       

#!/bin/bash

first=1

g++ -std=c++0x *.h test_st.cpp -o test_st
rm results* temp*
for cov in  10 15 20 25 30 35 40
do 
	for j in 33 50 66 99
	do
		for no_clusters in 16 29 54 81 104 131 153 172 200 225 250
		do
			temp=$(($no_clusters - 1))
			for i in $(seq 1 $temp);
			do
				./test_st $cov $i $no_clusters ../clusters/$no_clusters/ ./rep_traces $j 1 >> results-$cov-$j-$no_clusters-1
				#./test_st $cov $i $no_clusters '../clusters/200s-'$no_clusters'c/' ./rep_traces $j 1 >> results-$cov-$j-$no_clusters-1
			done
			mkdir ./results/data-phase-2-1/$no_clusters
			mv results_* ./results/data-phase-2-1/$no_clusters/.
			mv results-* ./results/data-phase-2-1/$no_clusters/.
		done
	done
done
#mv results-* ./results/200s-12c/.
#mv results_* ./results/200s-12c/.


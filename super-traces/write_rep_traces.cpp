#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "traces.h"
#include "packets.h"
#include "fileio.h"

using namespace std;

int main()
{
	stringstream rep_file, bfile, tfile, dst_bfile, dst_tfile;
	ifstream rep;
	for(int i = 1 ; i <= 500 ; i ++)
	{
		rep_file.str("");
		rep_file<<"./rep-traces/"<<i;
		rep.open(rep_file.str());
		string trace;
		for(int j = 1 ; j <= 25 ; j ++)
		{
			getline(rep, trace);
			//cout<<"Trace: "<<trace<<endl;
			bfile.str("");
			dst_bfile.str("");
			tfile.str("");
			dst_tfile.str("");
			bfile<<"./tail-less/tl_"<<i<<"_"<<trace<<".size";
			tfile<<"./tail-less/tl_"<<i<<"_"<<trace<<".time";
			dst_bfile<<"./tail-less-rep-traces/"<<i<<"_"<<j<<".size";
			dst_tfile<<"./tail-less-rep-traces/"<<i<<"_"<<j<<".time";
			cout<<"Reading from files: "<<bfile.str()<<" and "<<tfile.str()<<endl;
			cout<<"Writing to files: "<<dst_bfile.str()<<" and "<<dst_tfile.str()<<endl;
			write_to_disk(read_from_disk(bfile.str(), tfile.str()), dst_bfile.str(), dst_tfile.str());
		}
		rep.close();
	}
	return 0;
}

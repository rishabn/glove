#ifndef FILEIO_H
#define FILEIO_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "traces.h"

long long int findFileSize(std::string filename)
{
	long long int begin, end;
	std::ifstream fname(filename);
	begin = fname.tellg();
	fname.seekg(0, std::ios::end);
	end = fname.tellg();
	fname.close();
	return (end-begin);
}

void write_to_disk(trace src, std::string dst_bytes, std::string dst_times)
{
	std::ofstream dst_bfile, dst_tfile;
	dst_bfile.open(dst_bytes, std::ios::trunc|std::ios::out);
	dst_tfile.open(dst_times, std::ios::trunc|std::ios::out);
	if(src.length > 0)
	{
		for(int i = 0 ; i < src.length ; i ++)
		{
			dst_bfile<<src.packets[i].size*src.packets[i].direction<<std::endl;
			dst_tfile<<src.packets[i].time<<std::endl;
		}
	}
	else
	{
		dst_bfile<<"0"<<std::endl;
		dst_tfile<<"0"<<std::endl;
	}
	dst_bfile.close();
	dst_tfile.close();
}

trace read_from_disk(std::string src_bytes, std::string src_times)
{
	std::ifstream src_bfile, src_tfile;
	std::string bytes, times;
	long long int packet_count = 0, size, direction;
	double start_time, time;
	std::vector<packet> packets;
	trace tr;
	packet p;
	src_bfile.open(src_bytes);
	src_tfile.open(src_times);
	if((!src_bfile.is_open()) || (!src_tfile.is_open()))
		std::cout<<"Could not open source files to read from. SRC: "<<src_bytes<<std::endl;
	if(findFileSize(src_bytes) == 0 || findFileSize(src_times) == 0)
	{
		p = setPacket(0, 0, 1);
		packets.push_back(p);
		std::cout<<"File has no packets"<<std::endl;
	}	
	else while(!src_bfile.eof() && !src_tfile.eof())
	{
		getline(src_bfile, bytes);
		getline(src_tfile, times);
		//std::cout<<"Getting packet: "<<packet_count<<". Size: "<<bytes<<", Time: "<<times<<std::endl;
		if(packet_count == 0)
			start_time = stod(times);
		if(!bytes.empty() && !times.empty() && bytes != "" && times != "")
		{
			time = stod(times) - start_time;
			size = stoll(bytes);
			if(size < 0)
				direction = -1;
			else
				direction = 1;
			size = abs(size);
			p = setPacket(size, time, direction);
			packets.push_back(p);
		}
		packet_count ++;
	}
	if(packet_count == 0)
	{
		p = setPacket(0, 0, 1);
		packets.push_back(p);
	}
	tr = setTrace(packets, 0);
	src_bfile.close();
	src_tfile.close();
	return tr;
}

#endif

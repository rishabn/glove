
# In[135]:

def to_timeseries(site, trace_list_size, trace_list_time, step_size, ts_length):
    temp_up = np.zeros([ts_length])
    temp_down = np.zeros([ts_length])
    count = 0
    #print 'Computing TS for site: '+str(site)+'. TSLen: '+str(ts_length)+', Step: '+str(step_size)
    for i in range(1, ts_length):
        for j in range(0, len(trace_list_time[site])):
            if (i-1)*step_size <= trace_list_time[site][j] < i*step_size:
                if trace_list_size[site][j] < 0:
                    temp_up[i-1] = temp_up[i-1] + abs(trace_list_size[site][j])
                else:
                    temp_down[i-1] = temp_down[i-1] + abs(trace_list_size[site][j])
    #print temp_up
    #print temp_down
    return temp_up, temp_down


# In[136]:

def compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites):
    cluster_up = np.zeros([no_sites], dtype = float_)
    cluster_down = np.zeros([no_sites], dtype = float_)
    cluster_count = np.zeros([no_sites], dtype = float_)
    cluster_ttc = np.zeros([no_sites], dtype = float_)
    for i in range(0, no_clusters):
            for j in range(0, no_sites):
                if labels[j] == i:
                    cluster_count[i] = cluster_count[i] + 1
                    if up_array[j] > cluster_up[i]:
                        cluster_up[i] = up_array[j]
                    if down_array[j] > cluster_down[i]:
                        cluster_down[i] = down_array[j]
                    if ttc_array[j] > cluster_ttc[i]:
                        cluster_ttc[i] = ttc_array[j]
    
    total_defense_bytes = 0
    total_defense_time = 0
    for i in range(0, no_clusters):
        total_defense_bytes = total_defense_bytes + (cluster_count[i] * (cluster_up[i]+cluster_down[i]))
        total_defense_time = total_defense_time + (cluster_count[i] * cluster_ttc[i])
        
    total_bytes = 0
    total_time = 0
    for i in range(0, no_sites):
        total_bytes = total_bytes + (up_array[i]+down_array[i])
        total_time = total_time + (ttc_array[i])

    return total_defense_bytes/total_bytes, total_defense_time/total_time


# In[144]:

def levenshtein(seq1, seq2):
    oneago = None
    thisrow = range(1, len(seq2) + 1) + [0]
    for x in xrange(len(seq1)):
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in xrange(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
    return thisrow[len(seq2) - 1]


# In[137]:

import sklearn
from sklearn import *
import numpy as np
from numpy import *
import sys
from scipy.cluster.vq import *

#get_ipython().magic(u'pylab inline')

dir_ = './tail-less-rep-traces/'
from os import path

no_candidates = 1
no_sites = 200


size_array = np.array([], dtype = float_)
time_array = np.array([], dtype = float_)
len_array = np.zeros([no_sites], dtype = float_)
up_array = np.zeros([no_sites], dtype = float_)
down_array = np.zeros([no_sites], dtype = float_)
ttc_array = np.zeros([no_sites], dtype = float_)
min_step_size = np.zeros([no_sites], dtype=float)
trace_list_size = list()
trace_list_norm_size = list()
trace_list_time = list()

print ('Importing trace files to memory...')

for i in range(0, no_sites): 
    size_p =  str(i+1) + '_1.size'
    time_p =  str(i+1) + '_1.time'
    min_step_size[i] = infty
    size_path = path.join(dir_, size_p)
    time_path = path.join(dir_, time_p)
    temp_size = [float(l.strip()) for l in open(size_path).read().splitlines()]
    norm_temp_size = [float(l.strip()) for l in open(size_path).read().splitlines()]
    temp_time = [float(l.strip()) for l in open(time_path).read().splitlines()]
    if len(temp_size) != len(temp_time):
        print ('ERROR: UNEQUAL TRACES' + str(i))
    start_time = temp_time[0];
    for j in range(0, len(temp_time)):
        temp_time[j] = (temp_time[j] - start_time)
        ttc_array[i] = temp_time[j]
        if(j > 0 and temp_time[j]-temp_time[j-1] < min_step_size[i] and temp_time[j]-temp_time[j-1] > 1000 ):
            min_step_size[i] = abs(temp_time[j]-temp_time[j-1])
        if(temp_size[j] < 0):
            up_array[i] = up_array[i] + abs(temp_size[j])
            norm_temp_size[j] = abs(norm_temp_size[j])
        else:
            down_array[i] = down_array[i] + temp_size[j]
            norm_temp_size[j] = abs(norm_temp_size[j])
    trace_list_size.append(temp_size)
    trace_list_norm_size.append(norm_temp_size)
    trace_list_time.append(temp_time)
    len_array[i] = len(temp_size)


# Out[137]:

#     Populating the interactive namespace from numpy and matplotlib
#     Importing trace files to memory...
# 

# In[138]:

from pylab import *
import matplotlib.pyplot as plt
import mlpy
import sys
#get_ipython().magic(u'pylab inline')

print ('Computing DTW based distance matrix')
DTW_distance_up = np.zeros((no_sites, no_sites), dtype=int)
DTW_distance_down = np.zeros((no_sites, no_sites), dtype=int)
DTW_distance_norm = np.zeros((no_sites, no_sites), dtype=int)
for i in range(0, no_sites):
    print ('Computing Row: '+str(i))
    sys.stdout.flush()

    for j in range(i, no_sites):
        if(i == j):
            DTW_distance_up[i][j]=0
            DTW_distance_down[i][j]=0
            DTW_distance_norm[i][j]=0
        else:
            step_size = int(max(min_step_size[i], min_step_size[j]))
            ts_length = int((max(ttc_array[i], ttc_array[j])/step_size)+2)
            
            up_intervals_i = np.zeros([ts_length], dtype=int)
            up_intervals_j = np.zeros([ts_length], dtype=int)
            down_intervals_i = np.zeros([ts_length], dtype=int)
            down_intervals_j = np.zeros([ts_length], dtype=int)

            norm_intervals_i_up = np.zeros([ts_length], dtype=int)
            norm_intervals_j_up = np.zeros([ts_length], dtype=int)
            norm_intervals_i_down = np.zeros([ts_length], dtype=int)
            norm_intervals_j_down = np.zeros([ts_length], dtype=int)
            
            #print 'Step Size: '+str(step_size)
            #print 'TS Lengths: '+str(ts_length)
            
            up_intervals_i, down_intervals_i = to_timeseries(i, trace_list_size, trace_list_time, ts_length, step_size)
            up_intervals_j, down_intervals_j = to_timeseries(j, trace_list_size, trace_list_time, ts_length, step_size)

            DTW_distance_up[i][j] = mlpy.dtw_std(up_intervals_i, up_intervals_j, dist_only = True)
            DTW_distance_down[i][j] = mlpy.dtw_std(down_intervals_i, down_intervals_j, dist_only = True)
            DTW_distance_up[j][i] = DTW_distance_up[i][j]
            DTW_distance_down[j][i] = DTW_distance_down[i][j]
            DTW_distance_up[j][i] = DTW_distance_up[i][j]
            DTW_distance_down[j][i] = DTW_distance_down[i][j]
            
            norm_intervals_i_up, norm_intervals_i_down = to_timeseries(i, trace_list_norm_size, trace_list_time, ts_length, step_size)
            norm_intervals_j_up, norm_intervals_j_down = to_timeseries(j, trace_list_norm_size, trace_list_time, ts_length, step_size)

            DTW_distance_norm[i][j] = mlpy.dtw_std(norm_intervals_i_down, norm_intervals_j_down, dist_only = True)
            DTW_distance_norm[j][i] = DTW_distance_norm[i][j]            

            #print ('Computing DTW('+str(i)+', '+str(j)+'). Distance (Up/Down): '+str(DTW_distance_up[i][j])+'/'+str(DTW_distance_down[i][j]))
            #print ('Computing Normalized DTW('+str(i)+', '+str(j)+'). Distance: '+str(DTW_distance_norm[i][j]))
            #plt.plot(trace_list_time[i], trace_list_size[i], '-r')
            #plt.show()
            #plt.plot(trace_list_time[j], trace_list_size[j], '-b')
            #plt.show()

#print (DTW_distance_up);
#print (DTW_distance_down);
#print (DTW_distance_norm);

# Out[138]:

#     Populating the interactive namespace from numpy and matplotlib
#     Computing DTW based distance matrix
#     Computing DTW(0, 1). Distance (Up/Down): 10304/70403
#     Computing Normalized DTW(0, 1). Distance: 214070
#     Computing DTW(0, 2). Distance (Up/Down): 9554/133445
#     Computing Normalized DTW(0, 2). Distance: 308871

#get_ipython().magic(u'pylab inline')

#print 'Computing DLED based distance matrix'
#DLED_distance = np.zeros((no_sites, no_sites), dtype=int)
#for i in range(0, no_sites):
#    for j in range(i, no_sites):
#        if(i == j):
#            DLED_distance[i][j] = 0
#        else:
#            DLED_distance[i][j] = levenshtein(trace_list_size[i], trace_list_size[j])
#            DLED_distance[j][i] = DLED_distance[i][j]
#            print 'Computing DLED('+str(i)+', '+str(j)+'). Distance: '+str(DLED_distance[i][j])
#

# Out[145]:

#     Populating the interactive namespace from numpy and matplotlib
#     Computing DLED based distance matrix
#     Computing DLED(0, 1). Distance: 135
#     Computing DLED(0, 2). Distance: 154
#     Computing DLED(0, 3). Distance: 655
#     Computing DLED(0, 4). Distance: 65

from scipy.cluster.vq import vq, kmeans2, whiten
from scipy.cluster.hierarchy import *
no_clusters = 20

bandwidth = []
latency = []
labellist = []
method = []

#centroid,labels=kmeans2(data=DTW_distance_norm, k=no_clusters, iter=10000, minit='random')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("random/norm/k2")

#centroid,labels=kmeans2(data=DTW_distance_down, k=no_clusters, iter=10000, minit='random')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("random/down/k2")

#centroid,labels=kmeans2(data=DTW_distance_up, k=no_clusters, iter=10000, minit='random')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("random/up/k2")

#centroid,labels=kmeans2(data=DTW_distance_up+DTW_distance_down, k=no_clusters, iter=10000, minit='random')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("random/sum/k2")

centroid,labels=kmeans2(data=DTW_distance_norm, k=no_clusters, iter=10000, minit='points')
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
method.append("points/norm/k2")

centroid,labels=kmeans2(data=DTW_distance_down, k=no_clusters, iter=10000, minit='points')
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
method.append("points/down/k2")

centroid,labels=kmeans2(data=DTW_distance_up, k=no_clusters, iter=10000, minit='points')
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
method.append("points/up/k2")

centroid,labels=kmeans2(data=DTW_distance_up+DTW_distance_down, k=no_clusters, iter=10000, minit='points')
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
method.append("points/sum/k2")

leeway = 5

t = 1.0
linkage_mx=linkage(y=DTW_distance_norm, method='complete', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("complete/norm/h")
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("complete/norm/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_norm, method='average', metric='euclidean')
method.append("average/norm/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("average/norm/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_norm, method='centroid', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("centroid/norm/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
labellist.append(labels)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    t = t + .0001
    labellist.append(labels)
    bandwidth.append(bw_oh)
    method.append("centroid/norm/h")
    latency.append(l_oh)
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_norm, method='median', metric='euclidean')
method.append("median/norm/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("median/norm/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_norm, method='ward', metric='euclidean')
method.append("ward/norm/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("ward/norm/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_down, method='complete', metric='euclidean')
method.append("complete/down/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    method.append("complete/down/h")
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_down, method='average', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
method.append("average/down/h")
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    method.append("average/down/h")
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_down, method='centroid', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("centroid/down/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("centroid/down/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_down, method='median', metric='euclidean')
method.append("median/down/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("median/down/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_down, method='ward', metric='euclidean')
method.append("ward/down/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    method.append("ward/down/h")
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)


t = 1.0
linkage_mx=linkage(y=DTW_distance_up, method='complete', metric='euclidean')
method.append("complete/up/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("complete/up/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up, method='average', metric='euclidean')
method.append("average/up/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("average/up/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up, method='centroid', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("centroid/up/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    method.append("centroid/up/h")
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up, method='median', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
method.append("median/up/h")
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("median/up/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up, method='ward', metric='euclidean')
method.append("ward/up/h")
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    method.append("ward/up/h")
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)


t = 1.0
linkage_mx=linkage(y=DTW_distance_up+DTW_distance_down, method='complete', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("complete/sum/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    method.append("complete/sum/h")
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up+DTW_distance_down, method='average', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("average/sum/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("average/sum/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up+DTW_distance_down, method='centroid', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
method.append("centroid/sum/h")
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    method.append("centroid/sum/h")
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up+DTW_distance_down, method='median', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
labellist.append(labels)
method.append("median/sum/h")
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("median/sum/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)

t = 1.0
linkage_mx=linkage(y=DTW_distance_up+DTW_distance_down, method='ward', metric='euclidean')
labels=fcluster(linkage_mx, t = 1)
method.append("ward/sum/h")
labellist.append(labels)
bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
bandwidth.append(bw_oh)
latency.append(l_oh)
while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
    labels=fcluster(linkage_mx, t)
    labellist.append(labels)
    method.append("ward/sum/h")
    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
    bandwidth.append(bw_oh)
    latency.append(l_oh)
    t = t + .0001
labels = np.zeros(no_sites)


# Out[146]:

#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
# 

# In[147]:


#centroid,labels=kmeans2(data=DLED_distance, k=no_clusters, iter=10000, minit='random')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("random/DLED/k2")

#centroid,labels=kmeans2(data=DLED_distance, k=no_clusters, iter=10000, minit='points')
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, no_clusters, no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#labellist.append(labels)
#method.append("points/DLED/k2")


#t = 1.0
#linkage_mx=linkage(y=DLED_distance, method='complete', metric='euclidean')
#labels=fcluster(linkage_mx, t = 1)
#method.append("complete/DLED/h")
#labellist.append(labels)
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
#    labels=fcluster(linkage_mx, t)
#    method.append("complete/DLED/h")
#    labellist.append(labels)
#    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#    bandwidth.append(bw_oh)
#    latency.append(l_oh)
#    t = t + .0001
#labels = np.zeros(no_sites)
#
#t = 1.0
#linkage_mx=linkage(y=DLED_distance, method='average', metric='euclidean')
#labels=fcluster(linkage_mx, t = 1)
#method.append("average/DLED/h")
#labellist.append(labels)
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
#    labels=fcluster(linkage_mx, t)
#    labellist.append(labels)
#    method.append("average/DLED/h")
#    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#    bandwidth.append(bw_oh)
#    latency.append(l_oh)
#    t = t + .0001
#labels = np.zeros(no_sites)
#
#t = 1.0
#linkage_mx=linkage(y=DLED_distance, method='centroid', metric='euclidean')
#labels=fcluster(linkage_mx, t = 1)
#labellist.append(labels)
#method.append("centroid/DLED/h")
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
#    labels=fcluster(linkage_mx, t)
#    labellist.append(labels)
#    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#    bandwidth.append(bw_oh)
#    method.append("centroid/DLED/h")
#    latency.append(l_oh)
#    t = t + .0001
#labels = np.zeros(no_sites)
#
#t = 1.0
#linkage_mx=linkage(y=DLED_distance, method='median', metric='euclidean')
#labels=fcluster(linkage_mx, t = 1)
#labellist.append(labels)
#method.append("median/DLED/h")
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
#    labels=fcluster(linkage_mx, t)
#    labellist.append(labels)
#    method.append("median/DLED/h")
#    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#    bandwidth.append(bw_oh)
#    latency.append(l_oh)
#    t = t + .0001
#labels = np.zeros(no_sites)
#
#t = 1.0
#linkage_mx=linkage(y=DLED_distance, method='ward', metric='euclidean')
#labels=fcluster(linkage_mx, t = 1)
#method.append("ward/DLED/h")
#labellist.append(labels)
#bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#bandwidth.append(bw_oh)
#latency.append(l_oh)
#while(t < 2 and (max(labels) > no_clusters + leeway or max(labels) < no_clusters - leeway)):
#    labels=fcluster(linkage_mx, t)
#    labellist.append(labels)
#    method.append("ward/DLED/h")
#    bw_oh, l_oh = compute_overhead(labels, up_array, down_array, ttc_array, max(labels), no_sites)
#    bandwidth.append(bw_oh)
#    latency.append(l_oh)
#    t = t + .0001
#labels = np.zeros(no_sites)


# Out[147]:

#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
#     /usr/lib/python2.7/dist-packages/scipy/spatial/distance.py:1176: DeprecationWarning: using a non-integer number instead of an integer will result in an error in the future
#       dm = np.zeros((m * (m - 1) / 2,), dtype=np.double)
# 

# In[148]:

min_boh = infty
print (size(labellist), size(latency), size(bandwidth))
for i in range(0,size(bandwidth)):
    if no_clusters-leeway <= max(labellist[i]) <= no_clusters+leeway:
        bw_oh, l_oh = compute_overhead(labellist[i], up_array, down_array, ttc_array, max(labellist[i]), no_sites)
        if(min_boh > bw_oh):
            min_boh = bw_oh
            min_loh = l_oh
            min_labels = labellist[i]
            min_method = method[i]
print ('Minimum OH: '+str(min_boh)+'/'+str(min_loh)+', Clusters: '+str(max(min_labels)+1)+', Method: '+str(min_method))
print (min_labels)


# Out[148]:

#     7882700 78826 78826
#     Minimum OH: 1.14676459804/2.85410372555, Clusters: 10, Method: points/norm/k2
#     [6 7 5 1 7 6 2 6 6 5 5 3 1 6 7 5 3 1 3 5 5 5 3 8 3 9 4 5 5 2 3 7 4 1 4 2 7
#      8 4 6 8 7 4 9 9 1 6 4 5 6 4 7 6 6 6 5 9 8 4 5 9 9 8 4 1 6 5 9 9 2 7 3 2 4
#      1 0 2 5 4 3 2 9 1 4 4 5 9 9 4 1 9 5 9 2 7 5 8 2 7 5]
# 

# In[153]:

for i in range(0,max(min_labels)+1):
    filename = '/home/rishabn/Desktop/Papers/2014/WPES/gloves/code-wpes/clusters/'+str(i)+'200s.'+str(max(min_labels)+1)
    print ('Cluster '+str(i));
    f = open(filename, 'w')
    for j in range(0, no_sites):
        if min_labels[j] == i:
            f.write(str(j) + '\n')
            print(str(j)+'\n');
    f.close




#ifndef SUPERTRACE_LOGIC_H
#define SUPERTRACE_LOGIC_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>
#include "traces.h"
#include "fileio.h"
#include <limits.h>

using namespace std;

template <typename T> std::ostream& operator<<(std::ostream& os, std::vector<T> const& list) 
{
	typename std::vector<T>::const_iterator i = list.begin();
	if (i == list.end()) 
	{
		os << "[ ]";
		return os;
	}
	os << "[ " << *i ;
	++i;
	for (; i != list.end(); ++i) 
		os << ", " << *i ;
	os << "]\n";
	return os;
}

vector<int> compute_coverage(trace &super, vector<int> site_list, int cluster_no, int no_clusters, int min_coverage, int method)
{
	stringstream trace_path_bytes, trace_path_times;
	vector<vector<trace>> site_traces;
	vector<trace> temp_traces;
	vector<int> coverage(site_list.size());
	vector<long long int> input_bytes, input_times;
	for(int i = 0 ; i < site_list.size() ; i ++)
	{
		//cout<<"[LOG] Loading traces of site "<<site_list[i]<<" into memory"<<endl;
		for(int j = 1 ; j <= 50 ; j ++)
		{	
			trace_path_bytes<<"./tail-less/tl_"<<site_list[i]<<"_"<<j<<".size";
			trace_path_times<<"./tail-less/tl_"<<site_list[i]<<"_"<<j<<".time";
			temp_traces.push_back(read_from_disk(trace_path_bytes.str(), trace_path_times.str()));
			trace_path_bytes.str("");
			trace_path_times.str("");
			input_bytes.push_back(temp_traces[temp_traces.size()-1].total_bytes);
			input_times.push_back(temp_traces[temp_traces.size()-1].play_time);
		}
		site_traces.push_back(temp_traces);
		temp_traces.clear();
	}

	vector<long long int> comp_bytes(input_bytes.size()), comp_times(input_times.size());
	long long int total_bytes = 0, total_bytes_covered = 0, total_bytes_remaining = 0;
	double total_completion_times = 0, total_completion_times_covered = 0, total_time_remaining = 0;

	for(int i = 0 ; i < site_list.size() ; i ++)
	{
		//cout<<"[LOG] Computing coverage of site "<<site_list[i]<<endl;
		for(int j = 0 ; j < 50 ; j ++)
		{
			long long int st_cpkt_no = 0, st_cpkt_size = abs(super.packets[0].size); 
			double st_cpkt_time = super.packets[0].time;
			long long int tr_cpkt_no = 0, tr_cpkt_size = abs(site_traces[i][j].packets[0].size);
			double tr_cpkt_time = site_traces[i][j].packets[0].time;
			int st_cpkt_dir = super.packets[0].direction;
			int tr_cpkt_dir = site_traces[i][j].packets[0].direction;
			//cout<<"[LOG] Checking coverage of trace "<<j<<endl;
			int complete = 0;
			while(st_cpkt_no < super.packets.size() && complete == 0)
			{
				if(st_cpkt_dir == tr_cpkt_dir && st_cpkt_time >= tr_cpkt_time && st_cpkt_size > tr_cpkt_size)
				{
					//cout<<"[LOG] ST has more bytes than TR. Advancing TR."<<endl;
					//cout<<"[LOG] <Before> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
					//cout<<"[LOG] <Before> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					if(tr_cpkt_no < site_traces[i][j].packets.size())
					{
						st_cpkt_size -= tr_cpkt_size;
						tr_cpkt_no = tr_cpkt_no + 1;
						tr_cpkt_dir = site_traces[i][j].packets[tr_cpkt_no].direction;
						tr_cpkt_size = abs(site_traces[i][j].packets[tr_cpkt_no].size);
						tr_cpkt_time = site_traces[i][j].packets[tr_cpkt_no].time;
						//cout<<"[LOG] <After> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
						//cout<<"[LOG] <After> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					}
				}
				else if(st_cpkt_dir == tr_cpkt_dir && st_cpkt_time >= tr_cpkt_time && st_cpkt_size < tr_cpkt_size)
				{
					//cout<<"[LOG] TR has more bytes than ST. Advancing ST."<<endl;
					//cout<<"[LOG] <Before> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
					//cout<<"[LOG] <Before> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					if(st_cpkt_no < super.packets.size())
					{
						tr_cpkt_size -= st_cpkt_size;
						st_cpkt_no = st_cpkt_no + 1;
						st_cpkt_dir = super.packets[st_cpkt_no].direction;
						st_cpkt_size = abs(super.packets[st_cpkt_no].size);
						st_cpkt_time = super.packets[st_cpkt_no].time;
						//cout<<"[LOG] <After> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
						//cout<<"[LOG] <After> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					}
				}
				else if(st_cpkt_dir == tr_cpkt_dir && st_cpkt_time >= tr_cpkt_time && st_cpkt_size == tr_cpkt_size)
				{
					//cout<<"[LOG] TR has the same bytes as ST. Advancing ST and TR."<<endl;
					//cout<<"[LOG] <Before> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
					//cout<<"[LOG] <Before> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					if(st_cpkt_no < super.packets.size() && tr_cpkt_no < site_traces[i][j].packets.size())
					{
						st_cpkt_no = st_cpkt_no + 1;
						st_cpkt_dir = super.packets[st_cpkt_no].direction;
						st_cpkt_size = abs(super.packets[st_cpkt_no].size);
						st_cpkt_time = super.packets[st_cpkt_no].time;
						tr_cpkt_no = tr_cpkt_no + 1;
						tr_cpkt_dir = site_traces[i][j].packets[tr_cpkt_no].direction;
						tr_cpkt_size = abs(site_traces[i][j].packets[tr_cpkt_no].size);
						tr_cpkt_time = site_traces[i][j].packets[tr_cpkt_no].time;
						//cout<<"[LOG] <After> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
						//cout<<"[LOG] <After> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					}
				}
				else
				{
					//cout<<"[LOG] Time and Direction pre-conditions do not hold. Advancing ST."<<endl;
					//cout<<"[LOG] <Before> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
					//cout<<"[LOG] <Before> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					if(st_cpkt_no < super.packets.size())
					{
						st_cpkt_no = st_cpkt_no + 1;
						st_cpkt_dir = super.packets[st_cpkt_no].direction;
						st_cpkt_size = abs(super.packets[st_cpkt_no].size);
						st_cpkt_time = super.packets[st_cpkt_no].time;
						//cout<<"[LOG] <After> ST: #"<<st_cpkt_no<<", "<<st_cpkt_dir<<" x "<<st_cpkt_size<<" @ "<<st_cpkt_time<<endl;
						//cout<<"[LOG] <After> TR: #"<<tr_cpkt_no<<", "<<tr_cpkt_dir<<" x "<<tr_cpkt_size<<" @ "<<tr_cpkt_time<<endl;
					}
				}
				if(tr_cpkt_no == site_traces[i][j].packets.size() && complete == 0)
				{
					complete = 1;
					coverage[i] ++;
					comp_times[i*50 + j] = st_cpkt_time;
					for(int l = 0 ; l < st_cpkt_no ; l ++)
						comp_bytes[i*50+j] += abs(super.packets[l].size);
				}
			}
			//cout<<"[LOG] Packets of S"<<site_list[i]<<"T"<<j<<" covered: "<<tr_cpkt_no<<"/"<<site_traces[i][j].packets.size()<<endl;
			//if(tr_cpkt_no == site_traces[i][j].packets.size())
			//{ 
			//	coverage[i]++;
			//}
		}
		//cout<<"[LOG] Coverage of site "<<site_list[i]<<": "<<coverage[i]<<endl;
	}
	double es_bytes = 0, es_times = 0, ip_bytes = 0, ip_times = 0, reg_bytes = 0, reg_times = 0, tot_cov = 0;
	int cov_marker = 0;
	for(int i = 0 ; i < site_list.size() ; i ++)
	{
		if(coverage[i] >= min_coverage)
			cov_marker++;
		tot_cov += coverage[i];
	}
	for(int i = 0 ; i < comp_times.size() ; i ++)
	{
		es_bytes += comp_bytes[i];
		reg_bytes += super.total_bytes;
		es_times += comp_times[i];
		reg_times += comp_times[i];
		if(comp_bytes[i]!=0)
		{
			ip_bytes += input_bytes[i];
			ip_times += input_times[i];
		}
	}
	//cout<<"ES BOH WO CSB: "<<es_bytes/ip_bytes<<", STD BOH WO CSB: "<<reg_bytes/ip_bytes<<endl;
        ofstream output_csb, output_wocsb;
	stringstream csb, wocsb;
	csb<<"results_csb_"<<min_coverage<<".data";
	wocsb<<"results_wocsb_"<<min_coverage<<".data";
        //output_csb.open(csb.str(), ios::app);
	output_wocsb.open(wocsb.str(), ios::app);
	output_wocsb<<fixed; cout<<fixed;
	if(cov_marker == site_list.size())
	{
	               output_wocsb<<"Coverage: "<<min_coverage<<", Tot. Cov: "<<tot_cov<<", "<<cluster_no<<"/"<<no_clusters<<", STDB: "<<reg_bytes<<", IPB: "<<ip_bytes<<", STDT: "<<reg_times<<", IPT: "<<ip_times<<", BOH/LOH: "<<(double)reg_bytes/ip_bytes<<","<<(double)reg_times/ip_times<<", MAX:" <<method<<endl;
                       cout<<"Coverage: "<<min_coverage<<", Tot. Cov: "<<tot_cov<<", "<<cluster_no<<"/"<<no_clusters<<", STDB: "<<reg_bytes<<", IPB: "<<ip_bytes<<", STDT: "<<reg_times<<", IPT: "<<ip_times<<", BOH/LOH: "<<(double)reg_bytes/ip_bytes<<","<<(double)reg_times/ip_times<<", MAX:" <<method<<endl;
        }
	es_bytes = 0;
	reg_bytes = 0;
	ip_bytes = 0;
	ip_times = 0;
	es_times = 0;
	reg_times = 0;
	/*for(int i = 0 ; i < comp_times.size() ; i ++)
	{
		es_bytes += comp_bytes[i];
		reg_bytes += super.total_bytes;
		es_times += comp_times[i];
		reg_times += comp_times[i];
		ip_bytes += input_bytes[i];
		ip_times += input_times[i];
		if(comp_bytes[i]==0)
		{
			es_bytes += 3*input_bytes[i];
			es_times += 3*input_times[i];
			reg_bytes += 3*input_bytes[i];
			reg_times += 3*input_times[i];
		}
	}
	cout<<"ES BOH W CSB: "<<es_bytes/ip_bytes<<", STD BOH W CSB: "<<reg_bytes/ip_bytes<<endl;
	output_csb<<fixed;
	if(cov_marker == site_list.size())
	{
		if(method == 0)
			output_csb<<"Coverage: "<<min_coverage<<", Tot. Cov: "<<tot_cov<<", Cluster: "<<cluster_no<<"/"<<no_clusters<<", ESBytes W CSB: "<<es_bytes<<", STDBytes W CSB: "<<reg_bytes<<", IP Bytes W CSB: "<<ip_bytes<<", IS_MAX: "<<method;
		else
			output_csb<<", Tot. Cov: "<<tot_cov<<", ESBytes WO CSB: "<<es_bytes<<", STDBytes WO CSB: "<<reg_bytes<<", IP Bytes WO CSB: "<<ip_bytes<<", IS_MAX: "<<method<<endl;
	}*/
	output_wocsb.close();
	//output_csb.close();
	return coverage;
}

long long int min_coverage_under_thresh(int &site_coverage, vector<int> &coverage)
{
	if(coverage[min_element(coverage.begin(), coverage.end()) - coverage.begin()] >= site_coverage)
		return coverage.size();
	else
		return min_element(coverage.begin(), coverage.end()) - coverage.begin();
}

trace frontier_max(vector<trace> input, int &time_percentile);

trace frontier_max_no_upstream(vector<trace> input, int &time_percentile);

long long int countCompleteTraces(vector<int> &complete_flags)
{
        long long int count = 0;
        for(long long int i = 0 ; i < complete_flags.size() ; i ++)
                if(complete_flags[i] == 1)
                        count++;
        return count;
}

vector<int> whichTracesComplete(vector<int> &frontier, vector<trace> &t)
{
        vector<int> flags(t.size());
        for(long long int i = 0 ; i < t.size() ; i ++)
                if(frontier[i] == t[i].packets.size())
                        flags[i] = 1;
        return flags;
}


trace FrontierST(int &site_coverage, int &cluster_no, int &no_clusters, string &cluster_path, string &rep_trace_path, int &time_percentile, int is_max)
{
	trace output;
	
	ifstream cluster_input;
	stringstream input_path;
	input_path<<cluster_path<<"/corr-"<<cluster_no<<"";//<<no_clusters;
	cluster_input.open(input_path.str());
	//cout<<"[LOG] Opening cluster input file: "<<input_path.str()<<endl;
	input_path.str("");
	
	vector<int> cluster_members;
	string temp_string;

	while(!cluster_input.eof())
	{
		getline(cluster_input, temp_string);
		if(!temp_string.empty() && temp_string != "")
			cluster_members.push_back(atoi(temp_string.c_str()));
	}
	cluster_input.close();
	//cout<<"[LOG] Cluster Members: "<<cluster_members;

	vector<trace> composed_traces;
	vector<int> rep_counter(cluster_members.size());
	vector<int> coverage_tracker(cluster_members.size());	
	vector<vector<trace>> candidate_traces;
	vector<trace> inter_candidates;

	ifstream rep_traces;
	stringstream trace_path_bytes, trace_path_times;

	//cout<<"[LOG] Loading representative traces into memory"<<endl;
	for (int i = 0 ; i < cluster_members.size() ; i ++)
	{
		input_path<<"./rep-traces/"<<cluster_members[i];
		rep_traces.open(input_path.str());
		int count = 0;
		while(!rep_traces.eof())
		{
			count = count + 1;
			getline(rep_traces, temp_string);
			if(!temp_string.empty())
			{
				//cout<<"[LOG] "<<count<<"th rep-trace of site "<<cluster_members[i]<<": "<<temp_string<<endl;
				input_path.str("");
				trace_path_bytes<<"./tail-less/tl_"<<cluster_members[i]<<"_"<<temp_string<<".size";
				trace_path_times<<"./tail-less/tl_"<<cluster_members[i]<<"_"<<temp_string<<".time";
				//cout<<"[LOG] Source files path: "<<trace_path_bytes.str()<<endl;
				inter_candidates.push_back(read_from_disk(trace_path_bytes.str(), trace_path_times.str()));
				trace_path_bytes.str("");
				trace_path_times.str("");
			}
		}
		candidate_traces.push_back(inter_candidates);
		inter_candidates.clear();
		rep_traces.close();
	}

	//cout<<"[LOG] Testing coverage tracker"<<endl;
	//coverage_tracker = compute_coverage(candidate_traces[0][0], cluster_members);
	//cout<<"[LOG] Test Coverage results: "<<coverage_tracker;
	//cout<<"[LOG] Test min thresh: "<<min_coverage_under_thresh(site_coverage, coverage_tracker)<<endl;
	
	long long int min_cov_index = 0;

	while(min_cov_index < coverage_tracker.size() && rep_counter[min_cov_index] < candidate_traces[min_cov_index].size())
	{
		composed_traces.push_back(candidate_traces[min_cov_index][rep_counter[min_cov_index]]);
		if(is_max == 1)
		{
			output = frontier_max(composed_traces, time_percentile);
			coverage_tracker = compute_coverage(output, cluster_members, cluster_no, no_clusters, site_coverage, is_max);
			//cout<<"[LOG] Generated ST. Coverage: "<<coverage_tracker;
			//cout<<"[LOG] Rep Counter: "<<rep_counter;
			///getTrace(output);
		}
		else
		{
			output = frontier_max_no_upstream(composed_traces, time_percentile);
			coverage_tracker = compute_coverage(output, cluster_members, cluster_no, no_clusters, site_coverage, is_max);
			//cout<<"[LOG] Generated ST. Coverage: "<<coverage_tracker;
			//cout<<"[LOG] Rep Counter: "<<rep_counter;
			///getTrace(output);	
		}
		rep_counter[min_cov_index]++;
		min_cov_index = min_coverage_under_thresh(site_coverage, coverage_tracker);
		//cout<<"[LOG] Min Cov Site: "<<min_cov_index<<endl;
		//cout<<"[LOG] Site Coverage: "<<coverage_tracker;
		//printTraceChars(output);
	}	

	return output;
}

double percentile(int &time_percentile, vector<double> &nat, vector<int> &flags)
{
	vector<double> proj_times;
	for(int i = 0 ; i < nat.size() ; i ++)
		if(flags[i] == 0)
			proj_times.push_back(nat[i]);
	sort(proj_times.begin(), proj_times.end());
	return proj_times[floor(time_percentile*(proj_times.size())*.01)];
}

int quarter_direction(vector<trace> &input, vector<int> &frontier, vector<int> &flags)
{
	int incomplete = 0, count_updir = 0, count_downdir = 0;
	for(int i = 0 ; i < input.size() ; i ++)
	{
		if(input[i].packets[frontier[i]].direction == 1 && flags[i] == 0)
			count_updir++;
		else if(input[i].packets[frontier[i]].direction == -1 && flags[i] == 0)
			count_downdir++;
		if(flags[i] == 0)
			incomplete++;
	}
	if(count_downdir > .16*incomplete && count_downdir > 0)
		return -1;
	else
		return 1;
}

int majority_direction(vector<trace> &input, vector<int> &frontier, vector<int> &flags)
{
	int count_updir = 0, count_downdir = 0;
	for(int i = 0 ; i < input.size() ; i ++)
		if(input[i].packets[frontier[i]].direction == 1 && flags[i] == 0)
			count_updir++;
		else if(input[i].packets[frontier[i]].direction == -1 && flags[i] == 0)
			count_downdir++;
	if(count_updir >= count_downdir)
		return 1;
	else
		return -1;
}



double closest_to_projected_time(int &direction, vector<double> nat, double target_time, vector<int> &flags)
{
	vector<double> temp(flags.size());
	for(int i = 0 ; i < temp.size() ; i ++)
	{
		temp[i] = INT_MAX;
		if(flags[i] == 0)
			temp[i] = fabs(nat[i] - target_time);
	}
	return nat[min_element(temp.begin(), temp.end()) - temp.begin()];
}

long long int min_size(vector<trace> &input, vector<int> &frontier, int &direction, vector<double> &nat, double curr_time, vector<int> &flags)
{
	vector<long long int> temp(input.size());
	for(int i = 0 ; i < input.size() ; i ++)
	{
		temp[i] = LLONG_MAX;
		if(nat[i] <= curr_time && input[i].packets[frontier[i]].direction == direction && flags[i] == 0)
			temp[i] = abs(input[i].packets[frontier[i]].size);
	}
	return temp[min_element(temp.begin(), temp.end()) - temp.begin()];
}


long long int max_size(vector<trace> &input, vector<int> &frontier, int &direction, vector<double> &nat, double curr_time, vector<int> &flags)
{
	vector<long long int> temp(input.size());
	for(int i = 0 ; i < input.size() ; i ++)
		if(nat[i] <= curr_time && input[i].packets[frontier[i]].direction == direction && flags[i] == 0)
			temp[i] = abs(input[i].packets[frontier[i]].size);
	int diff = 0;
	diff = 50-(temp[max_element(temp.begin(), temp.end()) - temp.begin()]%50);
	return temp[max_element(temp.begin(), temp.end()) - temp.begin()] + diff;
}

trace frontier_max(vector<trace> input, int &time_percentile)
{
	vector<long long int> deficit(input.size());
	vector<int> frontier(input.size());
	vector<double> nat(input.size());
	vector<int> complete_flags(input.size());
	
	for(int i = 0 ; i < input.size() ; i ++)
		nat.push_back(input[i].packets[frontier[i]].time);
	vector<packet> st_packets;

	while(countCompleteTraces(complete_flags) < input.size())
	{
		double projected_time = percentile(time_percentile, nat, complete_flags);
		int curr_direction = quarter_direction(input, frontier, complete_flags);
		double curr_time = closest_to_projected_time(curr_direction, nat, projected_time, complete_flags);
		long long int curr_size = max_size(input, frontier, curr_direction, nat, curr_time, complete_flags);
		packet temp = setPacket(curr_size, curr_time, curr_direction);
		st_packets.push_back(temp);
		//cout<<"[LOG] New ST packet inserted: "<<endl;
		//getPacket(temp);
		double ipt = 0;
		
		for(long long int i = 0 ; i < input.size() ; i ++)
		{
			if(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time)
			{
				if(frontier[i] < input[i].packets.size()-1)
					ipt = input[i].packets[frontier[i] + 1].time - input[i].packets[frontier[i]].time;
				nat[i] += ipt;
				deficit[i] = curr_size - input[i].packets[frontier[i]].size;
				//cout<<"[LOG](Full) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE: "<<input[i].packets[frontier[i]].size<<endl;
				frontier[i]++;
			}
			while(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time && deficit[i] >= abs(input[i].packets[frontier[i]].size))
			{
				if(frontier[i] < input[i].packets.size()-1)
					ipt = input[i].packets[frontier[i] + 1].time - input[i].packets[frontier[i]].time;
				nat[i] += ipt;
				deficit[i] -= input[i].packets[frontier[i]].size;
				//cout<<"[LOG](Full) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE: "<<input[i].packets[frontier[i]].size<<endl;
				frontier[i]++;
			}
			if(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time && deficit[i]< input[i].packets[frontier[i]].size && deficit[i] > 0)
			{
				ipt = 0;
				input[i].packets[frontier[i]].size = input[i].packets[frontier[i]].size - deficit[i];
				//cout<<"[LOG](Part) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE (AFTER): "<<input[i].packets[frontier[i]].size<<endl;
				deficit[i] = 0;
			}
			nat[i] = curr_time + ipt;
		}
		complete_flags = whichTracesComplete(frontier, input);
	}
	return setTrace(st_packets, 1);
	
}

int isPIDirection(vector<int> &frontier, vector<trace> &input, int direction)
{
	for(int i = 0 ; i < input.size() ; i ++)
		if(input[i].packets[frontier[i]].direction == direction)
			return 1;
	return 0;
}
	
trace frontier_max_no_upstream(vector<trace> input, int &time_percentile)
{
	vector<long long int> deficit(input.size());
	vector<int> frontier(input.size()), complete_flags(input.size());
	vector<double> nat(input.size());

	for(int i = 0 ; i < input.size() ; i ++)
		nat.push_back(input[i].packets[frontier[i]].time);
	vector<packet> st_packets;

	while(countCompleteTraces(complete_flags) < input.size())
	{
		int curr_direction = 0 ;
		if(isPIDirection(frontier, input, -1))
			curr_direction = -1;
		else 
			curr_direction = 1;
	
		double projected_time = percentile(time_percentile, nat, complete_flags);
		double curr_time = closest_to_projected_time(curr_direction, nat, projected_time, complete_flags);
		long long int curr_size = max_size(input, frontier, curr_direction, nat, curr_time, complete_flags);
		packet temp = setPacket(curr_size, curr_time, curr_direction);
		st_packets.push_back(temp);
		//cout<<"[LOG] New ST packet inserted: "<<endl;
		//getPacket(temp);
		double ipt = 0;
		
		for(long long int i = 0 ; i < input.size() ; i ++)
		{
			if(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time)
			{
				if(frontier[i] < input[i].packets.size()-1)
					ipt = input[i].packets[frontier[i] + 1].time - input[i].packets[frontier[i]].time;
				nat[i] += ipt;
				deficit[i] = curr_size - input[i].packets[frontier[i]].size;
				//cout<<"[LOG](Full) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE: "<<input[i].packets[frontier[i]].size<<endl;
				frontier[i]++;
			}
			while(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time && deficit[i] >= abs(input[i].packets[frontier[i]].size))
			{
				if(frontier[i] < input[i].packets.size()-1)
					ipt = input[i].packets[frontier[i] + 1].time - input[i].packets[frontier[i]].time;
				nat[i] += ipt;
				deficit[i] -= input[i].packets[frontier[i]].size;
				//cout<<"[LOG](Full) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE: "<<input[i].packets[frontier[i]].size<<endl;
				frontier[i]++;
			}
			if(frontier[i] < input[i].packets.size() && input[i].packets[frontier[i]].direction == curr_direction && nat[i] <= curr_time && deficit[i]< input[i].packets[frontier[i]].size && deficit[i] > 0)
			{
				ipt = 0;
				input[i].packets[frontier[i]].size = input[i].packets[frontier[i]].size - deficit[i];
				//cout<<"[LOG](Part) Trace "<<i<<", Frontier @ "<<frontier[i]<<"/"<<input[i].packets.size();
                                //cout<<"[LOG] NAT: "<<nat[i]<<", IPT: "<<ipt<<", DEF: "<<deficit[i]<<", SIZE (AFTER): "<<input[i].packets[frontier[i]].size<<endl;
				deficit[i] = 0;
			}
			nat[i] = curr_time + ipt;
		}
		complete_flags = whichTracesComplete(frontier, input);
	}
	return setTrace(st_packets, 1);
	
}

#endif

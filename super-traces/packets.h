#ifndef PACKETS_H
#define PACKETS_H

#include <iostream>
#include <math.h>

struct packet
{
	long long int size;
	double time;
	int direction;
};

packet setPacket(long long int size, double time, int direction)
{
	packet p;
	p.size = abs(size);
	p.time = fabs(time);
	p.direction = direction;
	return p;
}

void getPacket(packet p)
{
	std::cout<<"Size: "<<p.size<<", Time: "<<p.time<<", Direction: "<<p.direction<<std::endl;
}

#endif

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "traces.h"
#include "packets.h"
#include "fileio.h"

using namespace std;

int main(int argc, char **argv)
{
	if(argc < 2)
	{
		cout<<"USAGE: "<<endl<<"ARG 1: <site-no>, ARG 2: <trace-no>"<<endl;
		return 1;
	}
	int site = atoi(argv[1]), trial = atoi(argv[2]);
	stringstream bfile, tfile;
	bfile<<"./input_data/"<<site<<"_"<<trial<<".cap.txt";
	tfile<<"./input_data/timeseq_"<<site<<"_"<<trial<<".cap.txt";
	trace tr;
	cout<<"Reading traces "<<bfile.str()<<" and "<<tfile.str()<<" from disk. "<<endl;
	tr = read_from_disk(bfile.str(), tfile.str());
	getTrace(tr);
	write_to_disk(tr, "byte_log.bytes", "time_log.times");
	return 0;
}

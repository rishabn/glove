
# In[39]:

import sklearn
from sklearn import *
from numpy import *
import sys
from scipy.cluster.vq import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
#get_ipython().magic(u'pylab inline')

dir_ = './input-data/.'

from os import path

for i in range(1, 502): 
    print ('Cutting tails of site: '+str(i));
    for j in range(1, 51): 
        size_p =  str(i) + '_' + str(j) + '.cap.size'
        time_p =  str(i) + '_' + str(j) + '.cap.time'
        size_path = path.join(dir_, size_p)
        time_path = path.join(dir_, time_p)
        temp_size = [float(l.strip()) for l in open(size_path).read().splitlines()]
        temp_time = [float(l.strip()) for l in open(time_path).read().splitlines()]
        if len(temp_size) != len(temp_time):
            print ('ERROR: UNEQUAL TRACES' + str(i))
        if len(temp_size) == 0:
# Uncomment if de-tailing the actual input data
            size_f = open('./tail-less/tl_'+str(i)+'_'+str(j)+'.size','w')
            time_f = open('./tail-less/tl_'+str(i)+'_'+str(j)+'.time','w')
            size_f.write('0\n')
            time_f.write('0\n')
            size_f.close
            time_f.close
        else:
            start_time = temp_time[0];
            cut_off = len(temp_time)
            for k in range(0, len(temp_time)):
                temp_time[k] = (temp_time[k] - start_time)
                if(temp_time[k] >= 10000000):
                    cut_off = k-1;
                    break;
            #print ('Cut off at: '+str(cut_off)+' of '+str(len(temp_time)))
            new_size = np.zeros(cut_off)
            new_time = np.zeros(cut_off)
# Uncomment if de-tailing the actual input data
            size_f = open('./tail-less/tl_'+str(i)+'_'+str(j)+'.size','w')
            time_f = open('./tail-less/tl_'+str(i)+'_'+str(j)+'.time','w')
            for l in range(0, cut_off):
                new_size[l] = temp_size[l]
                new_time[l] = temp_time[l]
                size_f.write(str(new_size[l])+'\n') 
                time_f.write(str(new_time[l])+'\n')             
            size_f.close()
            time_f.close()


# Out[39]:

#     Populating the interactive namespace from numpy and matplotlib
# 

# In[ ]:



